extends KinematicBody2D


export(int) var acceleration = 400
export(int) var max_speed = 80
export(int) var friction = 400
export(int) var roll_speed = 125
# Player direction vectors
var velocity = Vector2.ZERO
var roll_vector = Vector2.DOWN
# Player States
enum {
	MOVE,
	ROLL,
	ATTACK
}
var state = MOVE

# Node references
var animationPlayer = null
var animationTree = null
var animationState = null
var swordHitbox = null
var hurtbox = null
# var stats = PlayerStats



func _ready():
	animationPlayer = $AnimationPlayer
	animationTree = $AnimationTree
	animationState = animationTree.get("parameters/playback")
	animationTree.active = true

func _physics_process(delta):
	match state:
		MOVE:
			move_state(delta)
		ATTACK:
			attack_state(delta)

func move_state(delta):
	var input_vector = Vector2.ZERO
	input_vector.x = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	input_vector.y = Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")
	input_vector = input_vector.normalized()
	
	if input_vector != Vector2.ZERO:
		# roll_vector = input_vector
		# swordHitbox.knockback_vector = input_vector
		animationTree.set("parameters/Idle/blend_position", input_vector)
		animationTree.set("parameters/Walk/blend_position", input_vector)
		animationTree.set("parameters/Attack/blend_position", input_vector)
		# animationTree.set("parameters/Roll/blend_position", input_vector)
		animationState.travel("Walk")
		velocity = velocity.move_toward(input_vector * max_speed, acceleration * delta)
	else:
		velocity = velocity.move_toward(Vector2.ZERO, friction * delta)
		animationState.travel("Idle")
	
	move()

func attack_state(delta):
	pass

func move():
	velocity = move_and_slide(velocity)
