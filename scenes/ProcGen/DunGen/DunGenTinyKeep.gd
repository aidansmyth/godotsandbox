extends Node2D

var room = preload("res://scenes/ProcGen/DunGen/bits/Room.tscn")
var tile_size = 16
export var num_rooms = 50
export var min_size = 4
export var max_size = 10
export var h_spread = 100
export var cull = 0.5
var path # AStar pathfinding object
onready var map = $TileMap

var start_room = null
var end_room = null
var play_mode = null
var player = null


func _ready():
	randomize()
	make_rooms()

func _process(delta):
	update()

func _input(event):
	if event.is_action_pressed("ui_select"):
		for n in $Rooms.get_children():
			n.queue_free()
		path = null
		map.clear()
		make_rooms()
	if event.is_action_pressed("ui_focus_next"):
		make_map()

func make_rooms():
	for i in range(num_rooms):
		var pos = Vector2(rand_range(-h_spread, h_spread),0)
		var r = room.instance()
		var w = min_size + randi() % (max_size - min_size)
		var h = min_size + randi() % (max_size - min_size)
		r.make_room(pos, Vector2(w,h) * tile_size)
		$Rooms.add_child(r)
	# wait for rooms to stop moving
	yield(get_tree().create_timer(1.4), "timeout")
	# Cull rooms
	var room_positions = []
	for room in $Rooms.get_children():
		if randf() < cull:
			room.queue_free()
		else:
			room.mode = RigidBody2D.MODE_STATIC
			room_positions.append(Vector3(room.position.x, room.position.y, 0))
	yield(get_tree(), 'idle_frame')
	# generate spanning tree (path)
	path = find_mst(room_positions)

func find_mst(nodes):
	# Implement Prim's Algorithm
	var path = AStar.new()
	path.add_point(path.get_available_point_id(), nodes.pop_front())
	
	# repeat until no nodes remain
	while nodes:
		var min_dist = INF		# Minimun distace so far
		var min_p = null		# Position of that node
		var p = null			# Current position
		# Loop through all points in the path
		for p1 in path.get_points():
			p1 = path.get_point_position(p1)
			# Loop through the remaining nodes
			for p2 in nodes:
				if p1.distance_to(p2) < min_dist:
					min_dist = p1.distance_to(p2)
					min_p = p2
					p = p1
		var n = path.get_available_point_id()
		path.add_point(n, min_p)
		path.connect_points(path.get_closest_point(p), n)
		nodes.erase(min_p)
	return path
	

func _draw():
	for r in $Rooms.get_children():
		draw_rect(Rect2(r.position - r.size, r.size * 2), Color(32, 228, 0), false)
	
	if path:
		for p in path.get_points():
			for c in path.get_point_connections(p):
				var pp = path.get_point_position(p)
				var cp = path.get_point_position(c)
				draw_line(Vector2(pp.x, pp.y), Vector2(cp.x, cp.y), Color(1,1,0), 8, true)

func make_map():
	# Create a TileMap from the generated rooms and path
	map.clear()
	
	# Get map bounding box
	var full_rect = Rect2()
	for room in $Rooms.get_children():
		var r = Rect2(room.position - room.size, room.get_node("CollisionShape2D").shape.extents * 2)
		full_rect = full_rect.merge(r)
	var topleft = map.world_to_map(full_rect.position)
	var bottomright = map.world_to_map(full_rect.end)
	
	# Fill map with wall tile
	for x in range(topleft.x, bottomright.x):
		for y in range(topleft.y, bottomright.y):
			map.set_cell(x, y, 1)
			
	# Carve rooms
	var corridors = [] # 
	for room in $Rooms.get_children():
		var s = (room.size / tile_size).floor()
		var pos = map.world_to_map(room.position)
		var ul = (room.position / tile_size).floor() - s
		for x in range(2, s.x * 2 - 1):
			for y in range(2, s.y * 2 -1):
				map.set_cell(ul.x + x, ul.y + y, 0)
		# carve connecting corridor
		var p = path.get_closest_point(Vector3(room.position.x, room.position.y, 0))
		for conn in path.get_point_connections(p):
			if not conn in corridors:
				var start_point = map.world_to_map(Vector2(path.get_point_position(p).x, path.get_point_position(p).y ))
				var end_point = map.world_to_map(Vector2(path.get_point_position(conn).x, path.get_point_position(conn).y ))
				carve_path(start_point, end_point)
		corridors.append(p)
		
	# Update each cell's autotile bitmask
	map.update_bitmask_region(topleft, bottomright)

func carve_path(pos1, pos2):
	# Carve a path between two points
	var x_diff = sign(pos2.x - pos1.x)
	var y_diff = sign(pos2.y - pos1.y)
	if x_diff == 0: x_diff = pow(-1.0, randi() % 2)
	if y_diff == 0: y_diff = pow(-1.0, randi() % 2)
	# Choose either do x/y or y/x
	var x_y = pos1
	var y_x = pos2
	if (randi() % 2) > 0:
		x_y = pos2
		y_x = pos1
	for x in range(pos1.x, pos2.x, x_diff):
		map.set_cell(x, x_y.y, 0)
		map.set_cell(x, x_y.y + y_diff, 0) # Widen the corridor
	for y in range(pos1.y, pos2.y, y_diff):
		map.set_cell(y_x.x, y, 0)
		map.set_cell(y_x.x + x_diff, y , 0) # Widen the corridor
